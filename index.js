const express = require('express');
const port = 3000;
const app = express();

app.use(express.urlencoded());
app.use(express.json())
let database =[{
    name:"chester",
    password:"clavio"
}]
//////////#1
app.get('/home',(req,res)=>{
    res.send("Welcome to the home page")
})
//////////#3
app.get('/users',(req,res)=>{
    res.send(JSON.stringify(database))
})
//////////#5
app.delete('/delete-user',(req,res)=>{
    let {name}= req.body
    let find=database.filter(x=> x.name==name)
    if(find.length>0){
        database.splice(database.indexOf(name))
        res.send(`user ${name} has been deleted from the database`)
    }else{
        res.send(`user ${name} is not on the database`)
    }
    console.log(database);
})

app.listen(port,()=>{
    console.log(`Connected to port ${port}`)
});